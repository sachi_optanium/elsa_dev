import React from "react";
import * as _ from "lodash";
import loadable from '@loadable/component'
import { requestNet } from "./requestNet";

// const ELSA_INPUT = loadable(() => import("../components/ELSA_INPUT"))

const generateElement = (
  elemSchema,
  state,
  dispatch
) => {
  console.log("Generating Element",elemSchema.type);
  let resultElement = <div />;
  if (elemSchema.type == "input-text") {
    const ELSA_INPUT = loadable(() => import("../components/ELSA_INPUT"))
    resultElement = (
      <ELSA_INPUT
        key={elemSchema.key}
        type="text"
        value={_.get(state,elemSchema.model)}
        onChange={event => {
            console.log(state);
          dispatch(
            //_.set({},elemSchema.model,event.target.value)
            Object.fromEntries([[elemSchema.model, event.target.value]])
          );
        }}
      />
    );
    return resultElement;
  } else if (elemSchema.type == "input-password") {
    resultElement = (
      <input
      key={elemSchema.key}
        type="password"
        value={_.get(state,elemSchema.model)}
        onChange={event => {
          dispatch(
            //_.set({},elemSchema.model,event.target.value)
            Object.fromEntries([[elemSchema.model, event.target.value]])
          );
        }}
      />
    );
    return resultElement;
  } else if (elemSchema.type == "button-submit") {
    resultElement = (
      <button
      key={elemSchema.key}
        type="submit"
        onClick={() => {
          requestNet({
            action: elemSchema.action,
            payload: _.pick(state, elemSchema.payloadElems)
          });
        }}
      >
        {elemSchema.label}
      </button>
    );
    return resultElement;
  }
  else if (elemSchema.type == "button-process") {
    resultElement = (
      <button
      key={elemSchema.key}
        type="submit"
        onClick={() => {
            dispatch(
                //_.set({},elemSchema.model,event.target.value)
                {login : "Received From Server", child:{login:"Received child from Server"}}
              );
        }}
      >
        {elemSchema.label}
      </button>
    );
    return resultElement;
  }
  else if(elemSchema.type == "div"){
      resultElement=(<div key={elemSchema.key}>
          {elemSchema.children && elemSchema.children.map(c=>{
              return generateElement(c,state,dispatch)
          })}
      </div>)
      return resultElement;
  }
  console.log(resultElement);
  
};

export { generateElement };
