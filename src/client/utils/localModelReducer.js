import React from "react";
import * as _ from "lodash";
function reducer(state, newElem) {
    console.log("-----------",newElem);
    _.forIn(newElem,(val,key)=>{
        _.set(state,key,val)
    })
  return {...state};
}
const useLocalModelReducer = initModelData => {
  return React.useReducer(reducer, initModelData);
};

export { useLocalModelReducer };
