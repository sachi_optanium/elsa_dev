import React from 'react'
// eslint-disable-next-line import/no-extraneous-dependencies
import loadable from '@loadable/component'
import './main.css'



const LodableComponent = loadable(props => import(`./letters/${props.letter}`))



const App = (props) => {
  console.log("App Prop--->",props);
  let _props = props;
    if(props.isClient)
        _props = window.__serverData;
  console.log(_props);
  let letters = [_props.path?(_props.path.replace("/","")||"A"):"A"]
  
  return (
  <div>
    
    {/* {letters.map((l,i)=>{
      console.log(components[l])
      let c = components[l]
      console.log(c.render());
      return c.render({hello : "Sachi"})}
      )
    } */}

    {
      letters.map((l,i)=>{
        return <LodableComponent {..._props} key={i} letter={l}/>
      })
    }
  </div>
)}

export default App
