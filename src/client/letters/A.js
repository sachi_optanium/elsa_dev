// We simulate that "moment" is called in "A" and "B"
import React from "react";
import "./A.css";
import * as shortid from "shortid";
import TextField from "@material-ui/core/TextField";
import { useLocalModelReducer } from "../utils/localModelReducer";
import { generateElement } from "../utils/generateElement";

let elements = [
  {
    type: "input-text",
    key: shortid.generate(),
    model: "login",
    label: "username"
  },
  {
    type: "div",
    key: shortid.generate(),
    children: [
      {
        type: "input-text",
        key: shortid.generate(),
        model: "child.login",
        label: "username"
      },
      {
        type: "input-password",
        key: shortid.generate(),
        model: "child.password",
        label: "password"
      }
    ]
  },
  {
    type: "input-password",
    key: shortid.generate(),
    model: "password",
    label: "password"
  },
  {
    type: "button-submit",
    key: shortid.generate(),
    action: "login",
    payloadElems: ["login", "password"],
    label: "Login"
  },
  {
    type: "button-process",
    key: shortid.generate(),
    action: "get_userInfo",
    payloadElems: ["login", "password"],
    label: "Get"
  }
];

const A = props => {
  const [localModelState, localModelDispatch] = useLocalModelReducer({
    ...{
      login: "Initial",
      password: "Pass",
      child: { login: "childname", password: "childpass" }
    },
    ...props
  });
  const [elems, setElems] = React.useState(<div />);

  return (
    <div>
      {elements.map(elem =>
        generateElement(elem, localModelState, localModelDispatch)
      )}
      <div>{localModelState.login}</div>
    </div>
    
  );
};

export default A;
