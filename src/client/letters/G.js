import React from 'react'

const G = (props) => (
  <div>
  <span className="my-cool-class">
    {props.prefix}

    {' '}
    - G
  </span>
  <span>{JSON.stringify(props)}</span>
  </div>
)

export default G
