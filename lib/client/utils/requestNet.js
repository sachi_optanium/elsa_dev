"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.requestNet = void 0;

var axios = require("axios");

var requestNet = function (payload) {
  axios.post("/", payload, {
    withCredentials: true,
    maxRedirects: 0
  }).then(response => {
    console.log(response);

    if (response.status === 222) {
      window.location = response.data.routeTo;
    }
  });
};

exports.requestNet = requestNet;