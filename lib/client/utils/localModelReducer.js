"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLocalModelReducer = void 0;

var _react = _interopRequireDefault(require("react"));

var _ = _interopRequireWildcard(require("lodash"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function reducer(state, newElem) {
  console.log("-----------", newElem);

  _.forIn(newElem, (val, key) => {
    _.set(state, key, val);
  });

  return { ...state
  };
}

const useLocalModelReducer = initModelData => {
  return _react.default.useReducer(reducer, initModelData);
};

exports.useLocalModelReducer = useLocalModelReducer;