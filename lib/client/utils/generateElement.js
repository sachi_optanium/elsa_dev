"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateElement = void 0;

var _react = _interopRequireDefault(require("react"));

var _ = _interopRequireWildcard(require("lodash"));

var _component = _interopRequireDefault(require("@loadable/component"));

var _requestNet = require("./requestNet");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// const ELSA_INPUT = loadable(() => import("../components/ELSA_INPUT"))
const generateElement = (elemSchema, state, dispatch) => {
  console.log("Generating Element", elemSchema.type);

  let resultElement = _react.default.createElement("div", null);

  if (elemSchema.type == "input-text") {
    const ELSA_INPUT = (0, _component.default)({
      resolved: {},

      chunkName() {
        return "components-ELSA_INPUT";
      },

      isReady(props) {
        const key = this.resolve(props);

        if (this.resolved[key] === false) {
          return false;
        }

        if (typeof __webpack_modules__ !== 'undefined') {
          return !!__webpack_modules__[key];
        }

        return false;
      },

      importAsync: () => Promise.resolve().then(() => _interopRequireWildcard(require("../components/ELSA_INPUT"))),

      requireAsync(props) {
        const key = this.resolve(props);
        this.resolved[key] = false;
        return this.importAsync(props).then(resolved => {
          this.resolved[key] = true;
          return resolved;
        });
      },

      requireSync(props) {
        const id = this.resolve(props);

        if (typeof __webpack_require__ !== 'undefined') {
          return __webpack_require__(id);
        }

        return eval('module.require')(id);
      },

      resolve() {
        if (require.resolveWeak) {
          return require.resolveWeak("../components/ELSA_INPUT");
        }

        return eval('require.resolve')("../components/ELSA_INPUT");
      }

    });
    resultElement = _react.default.createElement(ELSA_INPUT, {
      key: elemSchema.key,
      type: "text",
      value: _.get(state, elemSchema.model),
      onChange: event => {
        console.log(state);
        dispatch( //_.set({},elemSchema.model,event.target.value)
        Object.fromEntries([[elemSchema.model, event.target.value]]));
      }
    });
    return resultElement;
  } else if (elemSchema.type == "input-password") {
    resultElement = _react.default.createElement("input", {
      key: elemSchema.key,
      type: "password",
      value: _.get(state, elemSchema.model),
      onChange: event => {
        dispatch( //_.set({},elemSchema.model,event.target.value)
        Object.fromEntries([[elemSchema.model, event.target.value]]));
      }
    });
    return resultElement;
  } else if (elemSchema.type == "button-submit") {
    resultElement = _react.default.createElement("button", {
      key: elemSchema.key,
      type: "submit",
      onClick: () => {
        (0, _requestNet.requestNet)({
          action: elemSchema.action,
          payload: _.pick(state, elemSchema.payloadElems)
        });
      }
    }, elemSchema.label);
    return resultElement;
  } else if (elemSchema.type == "button-process") {
    resultElement = _react.default.createElement("button", {
      key: elemSchema.key,
      type: "submit",
      onClick: () => {
        dispatch( //_.set({},elemSchema.model,event.target.value)
        {
          login: "Received From Server",
          child: {
            login: "Received child from Server"
          }
        });
      }
    }, elemSchema.label);
    return resultElement;
  } else if (elemSchema.type == "div") {
    resultElement = _react.default.createElement("div", {
      key: elemSchema.key
    }, elemSchema.children && elemSchema.children.map(c => {
      return generateElement(c, state, dispatch);
    }));
    return resultElement;
  }

  console.log(resultElement);
};

exports.generateElement = generateElement;