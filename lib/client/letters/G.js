"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const G = props => _react.default.createElement("div", null, _react.default.createElement("span", {
  className: "my-cool-class"
}, props.prefix, ' ', "- G"), _react.default.createElement("span", null, JSON.stringify(props)));

var _default = G;
exports.default = _default;