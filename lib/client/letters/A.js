"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

require("./A.css");

var shortid = _interopRequireWildcard(require("shortid"));

var _TextField = _interopRequireDefault(require("@material-ui/core/TextField"));

var _localModelReducer = require("../utils/localModelReducer");

var _generateElement = require("../utils/generateElement");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// We simulate that "moment" is called in "A" and "B"
let elements = [{
  type: "input-text",
  key: shortid.generate(),
  model: "login",
  label: "username"
}, {
  type: "div",
  key: shortid.generate(),
  children: [{
    type: "input-text",
    key: shortid.generate(),
    model: "child.login",
    label: "username"
  }, {
    type: "input-password",
    key: shortid.generate(),
    model: "child.password",
    label: "password"
  }]
}, {
  type: "input-password",
  key: shortid.generate(),
  model: "password",
  label: "password"
}, {
  type: "button-submit",
  key: shortid.generate(),
  action: "login",
  payloadElems: ["login", "password"],
  label: "Login"
}, {
  type: "button-process",
  key: shortid.generate(),
  action: "get_userInfo",
  payloadElems: ["login", "password"],
  label: "Get"
}];

const A = props => {
  const [localModelState, localModelDispatch] = (0, _localModelReducer.useLocalModelReducer)({ ...{
      login: "Initial",
      password: "Pass",
      child: {
        login: "childname",
        password: "childpass"
      }
    },
    ...props
  });

  const [elems, setElems] = _react.default.useState(_react.default.createElement("div", null));

  return _react.default.createElement("div", null, elements.map(elem => (0, _generateElement.generateElement)(elem, localModelState, localModelDispatch)), _react.default.createElement("div", null, localModelState.login));
};

var _default = A;
exports.default = _default;