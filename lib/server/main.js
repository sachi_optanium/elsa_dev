"use strict";

var _path = _interopRequireDefault(require("path"));

var _ = _interopRequireWildcard(require("lodash"));

var _express = _interopRequireDefault(require("express"));

var _react = _interopRequireDefault(require("react"));

var _server = require("react-dom/server");

var _server2 = require("@loadable/server");

var _user = require("./controller/user.controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const app = (0, _express.default)();
app.use(_express.default.static(_path.default.join(__dirname, '../../public')));

if (process.env.NODE_ENV !== 'production') {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  const {
    default: webpackConfig
  } = require('../../webpack.config.babel');

  const webpackDevMiddleware = require('webpack-dev-middleware');

  const webpack = require('webpack');
  /* eslint-enable global-require, import/no-extraneous-dependencies */


  const compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, {
    logLevel: 'silent',
    publicPath: '/dist/web',

    writeToDisk(filePath) {
      return /dist\/node\//.test(filePath) || /loadable-stats/.test(filePath);
    }

  }));
}

const nodeStats = _path.default.resolve(__dirname, '../../public/dist/node/loadable-stats.json');

const webStats = _path.default.resolve(__dirname, '../../public/dist/web/loadable-stats.json');

var servicesMap = {
  home: _user.getUserInfo
};
app.get('*', (req, res) => {
  const nodeExtractor = new _server2.ChunkExtractor({
    statsFile: nodeStats
  });
  const {
    default: App
  } = nodeExtractor.requireEntrypoint();
  let serverData = {};
  console.log(servicesMap, servicesMap[req.path.replace("/", "")]);
  if (req.path && _.has(servicesMap, req.path.replace("/", ""))) serverData = servicesMap[req.path.replace("/", "")]();
  serverData = _.merge(serverData, {
    path: req.path
  });
  const webExtractor = new _server2.ChunkExtractor({
    statsFile: webStats
  });
  const jsx = webExtractor.collectChunks(_react.default.createElement(App, serverData));
  const html = (0, _server.renderToString)(jsx);
  res.set('content-type', 'text/html');
  res.send(`
      <!DOCTYPE html>
      <html>
        <head>
        ${webExtractor.getLinkTags()}
        ${webExtractor.getStyleTags()}
        </head>
        <body>
        <script>
        window.__serverData = ${JSON.stringify(serverData)};
        </script>
          <div id="main">${html}</div>
          ${webExtractor.getScriptTags()}
        </body>
      </html>
    `);
});
app.use(_express.default.json());
app.post("*", (req, res) => {
  console.log(req.body, req.path);

  if (req.body.action == "login") {
    Promise.resolve().then(() => _interopRequireWildcard(require("./controller/user.controller"))).then(controller => {
      console.log(controller);
      controller.login(req, res);
    });
  } else {
    res.cookie("Something", "SomethingValue");
    res.status(222).send({
      routeTo: "/ghnghg"
    });
  }
}); // eslint-disable-next-line no-console

app.listen(9000, () => console.log('Server started http://localhost:9000'));